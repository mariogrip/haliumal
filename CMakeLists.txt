cmake_minimum_required(VERSION 3.0)
project(HaliumAl)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu99 -Wall -Wextra -Werror")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -Wall -fno-strict-aliasing -Wextra")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,--no-undefined")

include(FindPkgConfig)
include(GNUInstallDirs)

pkg_check_modules(LIBHARDWARE REQUIRED libhardware)
pkg_check_modules(LIBHYBRISVIBRATOR REQUIRED libvibrator)
pkg_search_module(LIBPROP REQUIRED libandroid-properties)
pkg_check_modules(ANDROID_HEADERS REQUIRED android-headers-22)

include_directories(include)
add_subdirectory(src)
add_subdirectory(demo)
