/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#include <haliumal/hardware/audio.h>
#include <iostream>

int main(int argc, char const *argv[]) {
  auto audio = new HaliumAl::Hardware::Audio();

  if (audio->initCheck())
    std::cout << "Audio initCheck success!" << '\n';
  else {
    std::cout << "Audio initCheck failed!" << '\n';
    return 1;
  }

  auto b = audio->openOutputStream();
  std::cout << "Output stream created :D" << '\n';
//  std::cout << "Master volume is" << audio->getMasterVolume() << '\n';
  return 0;
}
