/*
 * Copyright 2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */
#include <haliumal/hardware/lights.h>
#include <memory>

int main(int argc, char const *argv[]) {
  auto li = std::make_shared<HaliumAl::Hardware::Lights>();
  auto nn = li->create(HaliumAl::Hardware::Lights::NOTIFICATIONS);

  struct HaliumAl::Hardware::Light::LightState state;
  state.color = 1;

  nn->set(state);

  return 0;
}
