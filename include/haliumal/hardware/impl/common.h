/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#define IMPL_FACTORY(tclass) \
    extern "C" __attribute__ ((visibility ("default"))) tclass* CreateImplDevice() { return new tclass; } \
    extern "C" __attribute__ ((visibility ("default"))) void DestroyImplDevice(_class* d) { delete d; }

#define IMPL_CREATE_SYMBOL_NAME "CreateImplDevice"
#define IMPL_DESTROY_SYMBOL_NAME "DestroyImplDevice"
