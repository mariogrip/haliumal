/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#include "haliumal/common/enum.h"
#include "haliumal/hardware/impl/hw-impl.h"
#include "haliumal/hardware/lights.h"
#include <memory>

namespace HaliumAl {
  namespace Hardware {
  namespace Impl {
    class Lights : public HwImpl {
    public:
      virtual ~Lights() {};
      virtual Hardware::Lights::Type types() = 0;
      virtual std::shared_ptr<Impl::Light> create(Hardware::Lights::Type type) = 0;

      static std::shared_ptr<Impl::Lights> implCreate();
      static std::string implName() { return "Lights"; };
    };
    class Light {
    public:
      virtual ~Light() {};
      virtual Ret set(Hardware::Light::LightState state) = 0;
    };
  } /* impl */
}
} /* HaliumAl */
