/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */
 
#pragma once

#include "haliumal/common/enum.h"
#include <memory>

namespace HaliumAl {
  namespace Hardware {
  namespace Impl {
    class AudioStream {
    public:
      virtual ~AudioStream () {};
      virtual uint32_t getSampleRate() = 0;
      virtual size_t getBufferSize() = 0;
      virtual int standby() = 0;
      virtual int setParameters(const char *kv_pairs) = 0;
    };
    class AudioOutputStream {
    public:
      AudioOutputStream(std::shared_ptr<AudioStream> const i) : common(i) {}
      virtual ~AudioOutputStream() {};
      virtual uint32_t getLatency() = 0;
      std::shared_ptr<AudioStream> getCommonAudioStream() { return common; }
    private:
        std::shared_ptr<AudioStream> common;
    };
    class AudioInputStream {
    public:
      AudioInputStream(std::shared_ptr<AudioStream> const i) : common(i) {}
      virtual ~AudioInputStream() {};
      std::shared_ptr<AudioStream> getCommonAudioStream() { return common; }
    private:
        std::shared_ptr<AudioStream> common;
    };
    class Audio {
    public:
      virtual ~Audio() {};
      virtual Ret initCheck() = 0;
      virtual float getMasterVolume() = 0;
      virtual bool getMasterMute() = 0;
      virtual std::shared_ptr<AudioOutputStream> openOutputStream() = 0;
      virtual std::shared_ptr<AudioInputStream> openInputStream() = 0;

      static std::shared_ptr<Audio> create();
    };

  } /* impl */
  }
} /* HaliumAl */
