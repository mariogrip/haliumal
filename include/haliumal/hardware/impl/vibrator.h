/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#include "haliumal/common/enum.h"
#include "haliumal/hardware/impl/hw-impl.h"
#include <memory>

namespace HaliumAl {
  namespace Hardware {
  namespace Impl {
    class Vibrator : public HwImpl {
    public:
      virtual ~Vibrator() {};
      virtual Ret on(int msec) = 0;
      virtual Ret off() = 0;

      static std::shared_ptr<Vibrator> implCreate();
      static std::string implName() { return "Vibrator"; };
    };
  } /* impl */
  }
} /* HaliumAl */
