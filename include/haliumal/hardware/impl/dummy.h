/*
 * Copyright 2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#include "haliumal/hardware/impl/hw-impl.h"
#include "haliumal/common/enum.h"
#include <iostream>

#define DUMMY_FUNC(type, func, ret) func { \
 std::cout << "[Dummy" << #type << "] " << #func << " = " << #ret << std::endl; \
 return ret; \
} \

namespace HaliumAl {
namespace Hardware {
  namespace Impl {
    class Dummy : public HwImpl {
    public:
        Support support() override { return Support::DUMMY; };
        std::string name() override { return "Dummy"; };
    };
  }
}
}
