/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#include "haliumal/common/enum.h"
#include <memory>

#ifdef __cplusplus
namespace HaliumAl {
  namespace Hardware {
    namespace Impl {
      class Audio;
      class AudioStream;
      class AudioOutputStream;
      class AudioInputStream;
    } /* Impl */
    class AudioStream {
    public:
      AudioStream(std::shared_ptr<Impl::AudioStream> const& i) : impl(i) {};
      uint32_t getSampleRate();
      size_t getBufferSize();
      int standby();
      int setParameters(const char *kv_pairs);
    private:
      std::shared_ptr<Impl::AudioStream> impl;
    };
    class AudioOutputStream : public AudioStream {
    public:
      AudioOutputStream(std::shared_ptr<Impl::AudioOutputStream> const i);
    uint32_t getLatency();

    private:
      std::shared_ptr<Impl::AudioOutputStream> impl;
    };
    class AudioInputStream : public AudioStream {
    public:
      AudioInputStream(std::shared_ptr<Impl::AudioInputStream> const i);
    private:
      std::shared_ptr<Impl::AudioInputStream> impl;
    };
    class Audio {
    public:
      Audio();
      ~Audio() {};

      /**
        * check to see if the audio hardware interface has been initialized.
        * returns 0 on success, -ENODEV on failure.
        */
      Ret initCheck();
      float getMasterVolume();
      bool getMasterMute();
      std::shared_ptr<AudioOutputStream> openOutputStream();
      std::shared_ptr<AudioInputStream> openInputStream();
    private:
      std::shared_ptr<Impl::Audio> impl;
    };
  } /* Hardware */
} /* HaliumAl */


// C wrapper
extern "C" {
#endif /* __cplusplus */
  struct audio;
  struct audioOutputStream;
  struct audioInputStream;

  struct audio *audio_create();
  struct audio *audio_destroy(struct audio *a);
#ifdef __cplusplus
}
#endif
