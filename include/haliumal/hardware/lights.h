/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */
 
#pragma once

#include "haliumal/common/enum.h"
#include <memory>

namespace HaliumAl {
  namespace Hardware {
    namespace Impl {
      class Lights;
      class Light;
    } /* Impl */
    class Light {
    public:
      enum Flash : int32_t {
          /**
          * Keep the light steady on or off.
          */
          NONE,
          /**
          * Flash the light at specified rate.
          */
          TIMED,
          /**
          * Flash the light using hardware assist.
          */
          HARDWARE,
      };
      enum Brightness : int32_t {
          /**
          * Light brightness is managed by a user setting.
          */
          USER,
          /**
          * Light brightness is managed by a light sensor.
          */
          SENSOR,
          /**
          * Use a low-persistence mode for display backlights.
          *
          * When set, the device driver must switch to a mode optimized for low display
          * persistence that is intended to be used when the device is being treated as a
          * head mounted display (HMD). The actual display brightness in this mode is
          * implementation dependent, and any value set for color in LightState may be
          * overridden by the HAL implementation.
          *
          * For an optimal HMD viewing experience, the display must meet the following
          * criteria in this mode:
          * - Gray-to-Gray, White-to-Black, and Black-to-White switching time must be ≤ 3 ms.
          * - The display must support low-persistence with ≤ 3.5 ms persistence.
          *   Persistence is defined as the amount of time for which a pixel is
          *   emitting light for a single frame.
          * - Any "smart panel" or other frame buffering options that increase display
          *   latency are disabled.
          * - Display brightness is set so that the display is still visible to the user
          *   under normal indoor lighting.
          * - The display must update at 60 Hz at least, but higher refresh rates are
          *   recommended for low latency.
          *
          */
          LOW_PERSISTENCE,
      };
      /**
      * The parameters that can be set for a given light.
      *
      * Not all lights must support all parameters. If you
      * can do something backward-compatible, do it.
      */
      struct LightState {
          /**
          * The color of the LED in ARGB.
          *
          * Do your best here.
          *   - If your light can only do red or green, if they ask for blue,
          *     you should do green.
          *   - If you can only do a brightness ramp, then use this formula:
          *      unsigned char brightness = ((77*((color>>16)&0x00ff))
          *              + (150*((color>>8)&0x00ff)) + (29*(color&0x00ff))) >> 8;
          *   - If you can only do on or off, 0 is off, anything else is on.
          *
          * The high byte should be ignored. Callers will set it to 0xff (which
          * would correspond to 255 alpha).
          */
          uint32_t color;
          /**
          * To flash the light at a given rate, set flashMode to LIGHT_FLASH_TIMED,
          * and then flashOnMS should be set to the number of milliseconds to turn
          * the light on, followed by the number of milliseconds to turn the light
          * off.
          */
          Flash flashMode;
          int32_t flashOnMs;
          int32_t flashOffMs;
          Brightness brightnessMode;
      };

      Light(std::shared_ptr<Impl::Light> impl);
      ~Light() = default;

      Ret set(LightState state);
    private:
      std::shared_ptr<Impl::Light> m_impl;
    };

    class Lights {
    public:
      /**
      * These light IDs correspond to logical lights, not physical.
      * So for example, if your INDICATOR light is in line with your
      * BUTTONS, it might make sense to also light the INDICATOR
      * light to a reasonable color when the BUTTONS are lit.
      */
      enum Type : int32_t {
          BACKLIGHT,
          KEYBOARD,
          BUTTONS,
          BATTERY,
          NOTIFICATIONS,
          ATTENTION,
          BLUETOOTH,
          WIFI,
          COUNT,
      };

      Lights();
      ~Lights() {};

      Type types();
      std::shared_ptr<Light> create(Type type);
    private:
      std::shared_ptr<Impl::Lights> m_impl;
    };
  } /* Hardware */
} /* HaliumAl */
