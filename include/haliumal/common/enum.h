/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#include <string>

namespace HaliumAl {
  enum Ret { NO_ERR = 1, ERROR = -1, LIGHT_NOT_SUPPORTED, BRIGHTNESS_NOT_SUPPORTED };
  enum Support { DUMMY = 0, OK = 1, GOOD = 2, BEST = 3, UNSUPPORTED = -1 };

  inline std::string supportToStr(Support sup) {
    switch (sup) {
      case DUMMY: return "DUMMY";
      case OK: return "OK";
      case GOOD: return "GODD";
      case BEST: return "BEST";
      case UNSUPPORTED: return "UNSUPPORTED";
    }

    return "UNSUPPORTED";
  }

} /* HaliumAl */
