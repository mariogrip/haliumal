
/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#include "haliumal/hardware/audio.h"
#include "haliumal/hardware/impl/audio.h"

namespace hh = HaliumAl::Hardware;

hh::Audio::Audio() : impl(hh::Impl::Audio::create()) {}

HaliumAl::Ret hh::Audio::initCheck() {
  return impl->initCheck();
}

float hh::Audio::getMasterVolume() {
  return impl->getMasterVolume();
}

bool hh::Audio::getMasterMute() {
  return impl->getMasterVolume();
}

std::shared_ptr<hh::AudioOutputStream> hh::Audio::openOutputStream() {
  return std::make_shared<hh::AudioOutputStream>(impl->openOutputStream());
}

std::shared_ptr<hh::AudioInputStream> hh::Audio::openInputStream() {
  return std::make_shared<hh::AudioInputStream>(impl->openInputStream());
}

uint32_t hh::AudioStream::getSampleRate() {
  return impl->getSampleRate();
}

size_t hh::AudioStream::getBufferSize() {
  return impl->getBufferSize();
}

int hh::AudioStream::standby() {
  return impl->standby();
}

int hh::AudioStream::setParameters(const char *kv_pairs) {
  return impl->setParameters(kv_pairs);
}

hh::AudioInputStream::AudioInputStream(std::shared_ptr<hh::Impl::AudioInputStream> const i)
                                        : AudioStream(i->getCommonAudioStream()), impl(i) {};

hh::AudioOutputStream::AudioOutputStream(std::shared_ptr<hh::Impl::AudioOutputStream> const i)
                                        : AudioStream(i->getCommonAudioStream()), impl(i) {};

uint32_t hh::AudioOutputStream::getLatency() {
  return impl->getLatency();
}
