
/*
 * Copyright 2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#include "haliumal/hardware/lights.h"
#include "haliumal/hardware/impl/lights.h"

namespace hh = HaliumAl::Hardware;

// Lights class
hh::Lights::Lights() : m_impl(hh::Impl::Lights::implCreate()) {}

hh::Lights::Type hh::Lights::types() {
  return m_impl->types();
}

std::shared_ptr<hh::Light> hh::Lights::create(hh::Lights::Type type) {
    return std::make_shared<hh::Light>(m_impl->create(type));
}

// Light class
hh::Light::Light(std::shared_ptr<hh::Impl::Light> impl) : m_impl(impl) {}

HaliumAl::Ret hh::Light::set(hh::Light::LightState state) {
  return m_impl->set(state);
}
