/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#include "haliumal/hardware/impl/vibrator.h"
#include "vibrator-hybris.h"
//#include "vibrator-hidl.h"
#include "vibrator-dummy.h"
#include "../common.h"

#include <vector>

namespace hi = HaliumAl::Hardware::Impl;
namespace h = HaliumAl::Impl;

std::shared_ptr<hi::Vibrator> hi::Vibrator::implCreate() {
    std::vector<std::shared_ptr<hi::Vibrator>> impls{
      std::make_shared<hi::VibratorHybris>(),
      std::make_shared<hi::VibratorDummy>()
    };

    return h::Common::selectBestImpl<hi::Vibrator>(impls);
}
