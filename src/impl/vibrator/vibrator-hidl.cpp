/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#include "vibrator-hidl.h"

using namespace android::hardware::vibrator::V1_0;

namespace hi = HaliumAl::Impl;

hi::VibratorHidl::VibratorHidl() {
  dev = IVibrator::getService();
}

HaliumAl::Ret hi::VibratorHidl::on(int msec) {
  dev->on(msec);
}

HaliumAl::Ret hi::VibratorHidl::off() {
  dev->off();
}

HaliumAl::Support hi::VibratorHidl::isSupported() {
  if (IVibrator::getService())
    return Support::BEST;

  return Support::UNSUPPORTED;
}
