/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */
 
#include "vibrator-hybris.h"
#include "haliumal/version.h"

#include <stdio.h>
#include <stdlib.h>

namespace hi = HaliumAl::Hardware::Impl;

hi::VibratorHybris::VibratorHybris() {
  struct hw_module_t *hwmod;

  if (HaliumAl::Version::getMajorAndroidVersion() >= 7) {
    if (::hw_get_module(VIBRATOR_HARDWARE_MODULE_ID, (const hw_module_t **)(&hwmod)) < 0) {
      if (::vibrator_open(hwmod, &dev) < 0) {
              printf("ERROR: failed to open vibrator device\n");
              return;
      }
    }

    m_support = Support::OK;
  }
}

HaliumAl::Ret hi::VibratorHybris::on(int msec) {
  if (dev != NULL) {
    dev->vibrator_on(dev, msec);
  } else {
    ::vibrator_on(msec);
  }
}

HaliumAl::Ret hi::VibratorHybris::off() {
  if (dev != NULL) {
    dev->vibrator_off(dev);
  } else {
    ::vibrator_off();
  }
}
