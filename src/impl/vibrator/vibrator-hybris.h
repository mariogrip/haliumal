/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#include "haliumal/hardware/impl/vibrator.h"

#include "haliumal/common/enum.h"

#include <hardware/vibrator.h>
// Hack to allow both legacy and new hal
#undef _HARDWARE_VIBRATOR_H
#include <hardware_legacy/vibrator.h>
#include <memory>

namespace HaliumAl {
  namespace Hardware {
  namespace Impl {
    class VibratorHybris : public Vibrator {
    public:
      VibratorHybris();
      ~VibratorHybris() {};

      Ret on(int msec) override;
      Ret off() override;

      std::string name() { return "VibratorHybris"; }
    private:
      vibrator_device_t *dev = NULL;
    };
  } /* Impl */
  }
} /* HaliumAl */
