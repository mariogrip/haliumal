/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */
 
#pragma once

#include "haliumal/hardware/impl/vibrator.h"

#include "haliumal/common/enum.h"

#include <android/hardware/vibrator/1.0/IVibrator.h>

namespace HaliumAl {
  namespace Impl {
    class VibratorHidl : public Vibrator {
    public:
      VibratorHidl();
      ~VibratorHidl() {};

      static Support isSupported();

      Ret on(int msec) override;
      Ret off() override;
    private:
      android::sp<IVibrator> dev;
    };
  } /* Impl */
} /* HaliumAl */
