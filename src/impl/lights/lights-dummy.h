
/*
 * Copyright 2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#include "haliumal/hardware/impl/lights.h"
#include "haliumal/hardware/impl/dummy.h"

#include "haliumal/common/enum.h"

namespace HaliumAl {
  namespace Hardware {
  namespace Impl {
    class LightDummy : public Impl::Light {
    public:
      LightDummy() {};

      DUMMY_FUNC(Lights, Ret set(Hardware::Light::LightState) override, Ret::NO_ERR);
    };
  
    class LightsDummy : public Dummy, public Impl::Lights {
    public:
      LightsDummy() {};

        Support support() override { return Support::DUMMY; };
        std::string name() override { return "Dummy"; };

      DUMMY_FUNC(Lights, Hardware::Lights::Type types() override, Hardware::Lights::Type::NOTIFICATIONS);
      DUMMY_FUNC(Lights, std::shared_ptr<Impl::Light> create(Hardware::Lights::Type) override, std::make_shared<LightDummy>());
    };
  } /* Impl */
  }
} /* HaliumAl */
