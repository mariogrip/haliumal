
/*
 * Copyright 2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#include "lights-hybris.h"
#include "haliumal/version.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>

namespace hi = HaliumAl::Hardware::Impl;
namespace hh = HaliumAl::Hardware;

hi::LightsHybris::LightsHybris() {
  if (HaliumAl::Version::getMajorAndroidVersion() >= 7) {
    if (::hw_get_module(LIGHTS_HARDWARE_MODULE_ID, (const hw_module_t **)(&m_hwmod)) < 0) {
        printf("ERROR: failed to open lights device\n");
        return;
    }

    m_support = Support::OK;
  }
}

hi::LightsHybris::~LightsHybris() {
  if (m_hwmod) {
      m_hwmod = nullptr;
  }
}

hh::Lights::Type hi::LightsHybris::types() {
  return hh::Lights::Type::NOTIFICATIONS;
}

std::shared_ptr<hi::Light> hi::LightsHybris::create(hh::Lights::Type /* type */) {
  hw_device_t* device;
  int err = m_hwmod->methods->open(m_hwmod, LIGHT_ID_NOTIFICATIONS, &device);
  if (err != 0 || !device)
    throw "Error";
  return std::make_shared<hi::LightHybris>((light_device_t*) device);
}

hi::LightHybris::LightHybris(light_device_t* dev) : m_dev(dev) {}

hi::LightHybris::~LightHybris() {
  if (m_dev) {
      hw_device_t* device = (hw_device_t*) m_dev;
      device->close(device);
  }
}

HaliumAl::Ret hi::LightHybris::set(hh::Light::LightState state) {
  light_state_t androidState;
  memset(&androidState, 0, sizeof(light_state_t));
  androidState.color = state.color;
  androidState.flashMode = state.flashMode;
  androidState.flashOnMS = state.flashOnMs;
  androidState.flashOffMS = state.flashOffMs;
  androidState.brightnessMode = state.brightnessMode;

  if (!m_dev)
      return HaliumAl::Ret::ERROR;

  if (m_dev->set_light(m_dev, &androidState) != 0) {
        std::cout << "Failed to turn the light off" << std::endl;
        return HaliumAl::Ret::ERROR;
  }
  return HaliumAl::Ret::NO_ERR;
}
