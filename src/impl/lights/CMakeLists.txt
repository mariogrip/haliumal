add_library(LightsImpl STATIC
  lights.cpp
  lights-hybris.cpp
#  vibrator-hidl.cpp
)

target_include_directories(LightsImpl PRIVATE SYSTEM
  ${ANDROID_HEADERS_INCLUDE_DIRS}
)

target_link_libraries(LightsImpl
  ${LIBHARDWARE_LIBRARIES}
  ${ANDROID_PROPERTIES_LDFLAGS}
)
