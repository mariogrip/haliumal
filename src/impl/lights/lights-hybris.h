/*
 * Copyright 2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#include "haliumal/hardware/impl/lights.h"

#include "haliumal/common/enum.h"

extern "C" {
#include <hardware/lights.h>
}

namespace HaliumAl {
  namespace Hardware {
  namespace Impl {
    class LightHybris : public Impl::Light {
    public:
      LightHybris(light_device_t* dev);
      ~LightHybris();

      Ret set(Hardware::Light::LightState state) override;
    private:
      light_device_t *m_dev;
    };

    class LightsHybris : public Impl::Lights {
    public:
      LightsHybris();
      ~LightsHybris();

      Hardware::Lights::Type types() override;
      std::shared_ptr<Impl::Light> create(Hardware::Lights::Type type) override;

      std::string name() override { return "LightsHybris"; }
    private:
      hw_module_t *m_hwmod;
    };
  } /* Impl */
  }
} /* HaliumAl */
