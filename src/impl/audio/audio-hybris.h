// Copyright (c) 2019 Marius Gripsgard <marius@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "haliumal/hardware/impl/audio.h"

#include "haliumal/common/enum.h"

extern "C" {
#include <hardware/audio.h>
}

#include <memory>

namespace HaliumAl {
  namespace Hardware {
  namespace Impl {
    class AudioHybris : public Audio {
    public:
      AudioHybris();
      ~AudioHybris();

      static Support isSupported();

      Ret initCheck() override;
      float getMasterVolume() override;
      bool getMasterMute() override;
      std::shared_ptr<AudioOutputStream> openOutputStream();
      std::shared_ptr<AudioInputStream> openInputStream();
    private:
      audio_hw_device *_dev = NULL;
    };
    class AudioStreamHybris : public AudioStream {
    public:
      AudioStreamHybris (audio_stream *stream) : _stream(stream) {};
      ~AudioStreamHybris () {};
      uint32_t getSampleRate() override;
      size_t getBufferSize() override;
      int standby() override;
      int setParameters(const char *kv_pairs) override;
    private:
      audio_stream *_stream = NULL;
    };
    class AudioOutputStreamHybris : public AudioOutputStream {
    public:
      AudioOutputStreamHybris(audio_stream_out *dev, audio_hw_device *hwdev)
                              : AudioOutputStream(std::make_shared<AudioStreamHybris>(&dev->common)), _dev(dev), _hwdev(hwdev) {};
      ~AudioOutputStreamHybris();
      uint32_t getLatency() override;

      private:
        audio_stream_out *_dev = NULL;
        audio_hw_device *_hwdev = NULL;
    };
    class AudioInputStreamHybris : public AudioInputStream {
    public:
      AudioInputStreamHybris(audio_stream_in *dev, audio_hw_device *hwdev)
                            : AudioInputStream(std::make_shared<AudioStreamHybris>(&dev->common)), _dev(dev), _hwdev(hwdev) {};
      ~AudioInputStreamHybris();

    private:
      audio_stream_in *_dev = NULL;
      audio_hw_device *_hwdev = NULL;
    };
  } /* Impl */
  }
} /* HaliumAl */
