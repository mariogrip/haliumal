// Copyright (c) 2019 Marius Gripsgard <marius@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "audio-hybris.h"
#include "haliumal/version.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

/* Workaround for MTK */
#define AUDIO_HARDWARE_MODULE_ID2 "libaudio"

namespace hi = HaliumAl::Hardware::Impl;

hi::AudioHybris::AudioHybris() {
  struct hw_module_t *hwmod;

  if (::hw_get_module_by_class(AUDIO_HARDWARE_MODULE_ID,
                               AUDIO_HARDWARE_MODULE_ID_PRIMARY,
                               (const hw_module_t **)(&hwmod)) < 0) {
    std::cout << "Warning: faled to find module by class, trying alternative" << '\n';
    if (::hw_get_module_by_class(AUDIO_HARDWARE_MODULE_ID2,
      AUDIO_HARDWARE_MODULE_ID_PRIMARY,
      (const hw_module_t**) &hwmod) < 0) {
        std::cout << "FAILED TO GET MODULE AT ALL!" << '\n';
      }
  }

  if (::audio_hw_device_open(hwmod, &_dev) < 0) {
    std::cout << "ERROR: failed to open audio device" << '\n';
  }
}

hi::AudioHybris::~AudioHybris() {
  if (_dev) {
    audio_hw_device_close(_dev);
  }
}

HaliumAl::Ret hi::AudioHybris::initCheck() {
  int a = _dev->init_check(_dev);
  if (a == 0)
    return NO_ERR;
  return ERROR;
}

float hi::AudioHybris::getMasterVolume() {
  float vol;
  _dev->get_master_volume(_dev, &vol);
  return vol;
}

bool hi::AudioHybris::getMasterMute() {
  bool mute;
  _dev->get_master_mute(_dev, &mute);
  return mute;
}

HaliumAl::Support hi::AudioHybris::isSupported() {
  if (::hw_get_module(AUDIO_HARDWARE_MODULE_ID, NULL) < 0)
    return Support::OK;

  return Support::UNSUPPORTED;
}

std::shared_ptr<hi::AudioOutputStream> hi::AudioHybris::openOutputStream() {
  struct audio_config config_out = {
    .sample_rate = 44100,
    .channel_mask = AUDIO_CHANNEL_OUT_STEREO,
    .format = AUDIO_FORMAT_PCM_16_BIT
  };
  struct audio_stream_out *stream_out = NULL;
  _dev->open_output_stream(_dev, 0, AUDIO_DEVICE_OUT_DEFAULT,
      AUDIO_OUTPUT_FLAG_PRIMARY, &config_out, &stream_out, NULL
  );
  return std::make_shared<hi::AudioOutputStreamHybris>(stream_out, _dev);
}

std::shared_ptr<hi::AudioInputStream> hi::AudioHybris::openInputStream() {
  struct audio_config config_in = {
		.sample_rate = 48000,
		.channel_mask = AUDIO_CHANNEL_IN_STEREO,
		.format = AUDIO_FORMAT_PCM_16_BIT
	};
	struct audio_stream_in *stream_in = NULL;

	_dev->open_input_stream(_dev, 0, AUDIO_DEVICE_IN_DEFAULT,
			&config_in, &stream_in
			, AUDIO_INPUT_FLAG_NONE, NULL, AUDIO_SOURCE_DEFAULT
  );
  return std::make_shared<hi::AudioInputStreamHybris>(stream_in, _dev);
}

uint32_t hi::AudioStreamHybris::getSampleRate() {
  return _stream->get_sample_rate(_stream);
}

size_t hi::AudioStreamHybris::getBufferSize() {
  return _stream->get_buffer_size(_stream);
}

int hi::AudioStreamHybris::standby() {
  return _stream->standby(_stream);
}

int hi::AudioStreamHybris::setParameters(const char *kv_pairs) {
  return _stream->set_parameters(_stream, kv_pairs);
}

uint32_t hi::AudioOutputStreamHybris::getLatency() {
  return _dev->get_latency(_dev);
}

hi::AudioOutputStreamHybris::~AudioOutputStreamHybris() {
  _hwdev->close_output_stream(_hwdev, _dev);
  std::cout << "closed strem" << '\n';
}

hi::AudioInputStreamHybris::~AudioInputStreamHybris() {
  _hwdev->close_input_stream(_hwdev, _dev);
}

//IMPL_FACTORY(HaliumAl::Impl::AudioHybris);
extern "C" __attribute__ ((visibility ("default"))) hi::AudioHybris* CreateImplDevice() { return new hi::AudioHybris; }
