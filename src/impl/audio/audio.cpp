// Copyright (c) 2019 Marius Gripsgard <marius@ubports.com>
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "haliumal/hardware/impl/audio.h"
#include "audio-hybris.h"
//#include "vibrator-hidl.h"
#include "../common.h"

namespace hi = HaliumAl::Hardware::Impl;

std::shared_ptr<hi::Audio> hi::Audio::create() {
//  if (hi::AudioHidl::isSupported())
//    return std::make_shared<hi::AudioHidl>();

  return HaliumAl::Impl::Common::selectVersionedLib<hi::AudioHybris>("libAudioImpl");
}
