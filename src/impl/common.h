/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#include <memory>
#include "haliumal/version.h"
#include "haliumal/hardware/impl/common.h"
#include <dlfcn.h>
#include <sstream>
#include <vector>
#include <iostream>

namespace HaliumAl {
  namespace Impl {
    class Common {
    public:
      template<typename T>
      static std::shared_ptr<T> openLib(std::string lib_str) {
        void* lib = dlopen(lib_str.c_str(), RTLD_LAZY);

        T* (*create)();
        void (*destroy)(T*);

        create = (T* (*)())dlsym(lib, IMPL_CREATE_SYMBOL_NAME);
        destroy = (void (*)(T*))dlsym(lib, IMPL_DESTROY_SYMBOL_NAME);

        return std::shared_ptr<T>((T*)create(),
          [destroy](T* dev) {
            if (dev)
              destroy(dev);
          });
      }
      template<typename T>
      static std::shared_ptr<T> selectVersionedLib(std::string lib_str) {
        auto sdk = HaliumAl::Version::getSdkVersion();
        std::stringstream lib_str_version;
        lib_str_version << lib_str << "-" << sdk << ".so";
        std::string p = lib_str_version.str();
        return openLib<T>(p);
      }

      template<typename T>
      static std::shared_ptr<T> selectBestImpl(std::vector<std::shared_ptr<T>> impls) {
        std::shared_ptr<T> best;
        for (auto impl : impls) {
          std::cout << "Probe: " << impl->name() << " support level: " << supportToStr(impl->support()) << std::endl;
          if (impl->support() == Support::UNSUPPORTED)
            continue;

          if (!best) {
            best = impl;
            continue;
          }

          if (impl->support() > best->support())
            best = impl;
        }

        if (!best) {
          std::stringstream err;
          err << "Did not find a impl for: " << T::implName();
          throw std::runtime_error(err.str());
        }

        std::cout << "Probe: Selected " << best->name() << " as impl for " << T::implName() << std::endl;

        return best;
      }
    };
  } /* Impl */
} /* HaliumAl */
