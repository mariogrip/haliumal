
/*
 * Copyright 2019-2021 UBports foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marius Gripsgard <marius@ubports.com>
 */

#include "haliumal/version.h"
#include <hybris/properties/properties.h>

int HaliumAl::Version::getSdkVersion() {
  char value[PROP_VALUE_MAX] = "";
  ::property_get("ro.build.version.sdk", value, "22");

  return std::stoi(value);
}

std::tuple<int, int, int> HaliumAl::Version::getAndroidVersion() {
  char value[PROP_VALUE_MAX] = "";
  ::property_get("ro.build.version.release", value, "4.1.1");

  std::tuple<int, int, int> ret{0, 0, 0};
  if (sscanf(value, "%d.%d.%d", &std::get<0>(ret), &std::get<1>(ret), &std::get<2>(ret)) == 3)
      return ret;
  else
    return std::make_tuple(4, 1, 1);
}

int HaliumAl::Version::getMajorAndroidVersion() {
    return std::get<0>(getAndroidVersion());
}
