# HaliumAl

HaliumAl is an abstraction layer to access different hardware components


HaliumAl will also allow creation device/version specific of implementation of
different hardware components.

HaliumAl will guarantee ABI and API stability

HaliumAl will manage memory if c++ is used

# Why

This is needed to unify all our different devices to use one rootfs to rule them
all. It will also be needed for transition to hidl/treble. It will also allow
to simply switch between Android based devices and Linux kernel based

Why c++? Since hidl is build with C++ we have to use C++ since there is no
c api for it. but also we get all the goodies like std::shared_ptr etc with c++
that you dont get with c.

# API

Main API is written in C++, but a wrapper for C will be created.
We also want to keep the API as simple as possible

NOTE! This is still under development so API/ABI stability is hugely UNSTABLE!

# Status

- Vibrator:
  - hidl: OK
  - hybris: OK
